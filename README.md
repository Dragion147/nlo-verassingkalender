# NLO-VerassingKalender

Client ideas:
- VueJs application integrated with the Server to display information and ability to interact with the game.
- Maybe use Vuex or SignalR to have a bit more dynamic interaction and give better feedback on the current state of the game.

Server ideas:
- C# project that does the heavy lifting
- Unit tests
- Validation of moves
- Prevent the same user of playing the same round/game twice

Current status:
- Server solution can be opened in Visual Studio and project should be able to be ran right away. This should open a swagger page if not browse to localhost:{port}/swagger. The server can be verified working using the swagger page, postman or the browser url (all methods are get requests).
- Unit tests may be ran to test if code works and does what it should.
- In appsettings, the grid size / prize list may be altered.
