using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using VerassingsKalender.Interfaces;
using VerassingsKalender.Models;

namespace VerassingsKalender.Test
{
    [TestClass]
    public class GridTests
    {
        [DataTestMethod]
        [DataRow(10, 10)]
        [DataRow(10, 30)]
        [DataRow(30, 10)]
        [DataRow(100,100)]
        public void TestGridCreation(int x, int y)
        {
            var grid = new Grid(new GridOptions()
            {
                Height = y,
                Width = x
            });
            grid.RecreateGrid();

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    var cell = grid.Cells[i][j];
                    Assert.IsNotNull(cell);
                    Assert.IsFalse(cell.Revealed);
                }
            }
        }

        [DataTestMethod]
        [DataRow(-1, 10)]
        [DataRow(10, -1)]
        [DataRow(-1, -1)]
        public void TestInvalidGridCreation(int x, int y)
        {
            var grid = new Grid(new GridOptions()
            {
                Height = y,
                Width = x
            });

            Assert.ThrowsException<ArgumentException>(() => grid.RecreateGrid());

        }



        [DataTestMethod]
        [DataRow(10, 10)]
        [DataRow(10, 30)]
        [DataRow(30, 10)]
        [DataRow(100, 100)]
        public void TestSimplePrizeCreation(int prize, int amount)
        {
            var grid = new Grid(new GridOptions()
            {
                Height = 50,
                Width = 50,
                Prizes = new List<Prize>()
                {
                    new Prize(){ Amount = prize, Frequency = amount}
                }
            });
            grid.RecreateGrid();

            int prizesSet = 0;
            for (int i = 0; i < 50; i++)
            {
                for (int j = 0; j < 50; j++)
                {
                    var cell = grid.Cells[i][j];
                    Assert.IsNotNull(cell);
                    Assert.IsFalse(cell.Revealed);
                    if (cell.Prize.HasValue && cell.Prize.Value > 0)
                    {
                        Assert.AreEqual(prize, cell.Prize.Value);
                        prizesSet++;
                    }
                }
            }

            Assert.AreEqual(amount, prizesSet);
        }

        [DataTestMethod]
        [DataRow(10, 30)]
        [DataRow(25, 5)]
        public void TestMultiPrizeCreation(int prizeA, int prizeB)
        {
            var grid = new Grid(new GridOptions()
            {
                Height = 50,
                Width = 50,
                Prizes = new List<Prize>()
                {
                    new Prize(){ Amount = prizeA, Frequency = 10 },
                    new Prize(){ Amount = prizeB, Frequency = 10 }
                }
            });
            grid.RecreateGrid();

            int prizeASet = 0;
            int prizeBSet = 0;

            for (int i = 0; i < 50; i++)
            {
                for (int j = 0; j < 50; j++)
                {
                    var cell = grid.Cells[i][j];
                    Assert.IsNotNull(cell);
                    Assert.IsFalse(cell.Revealed);
                    if (cell.Prize.HasValue)
                    {
                        if(cell.Prize.Value == prizeA)
                        {
                            prizeASet++;
                        }

                        if(cell.Prize.Value == prizeB)
                        {
                            prizeBSet++;
                        }
                    }
                }
            }

            Assert.AreEqual(10, prizeASet);
            Assert.AreEqual(10, prizeBSet);
        }

        [DataTestMethod]
        [DataRow(1,1, true)]
        [DataRow(45, 55, false)]
        [DataRow(55, 45, false)]
        [DataRow(55, 55, false)]
        [DataRow(-1, 55, false)]
        [DataRow(55, -1, false)]

        public void TestValidMoves(int x, int y, bool expected)
        {
            var grid = new Grid(new GridOptions()
            {
                Height = 50,
                Width = 50
            });
            grid.RecreateGrid();

            var isValid = grid.IsValidMove(x, y);

            Assert.AreEqual(expected, isValid);
        }

        [TestMethod]
        public void TestCellRevealing()
        {
            var grid = new Grid(new GridOptions()
            {
                Height = 50,
                Width = 50
            });
            grid.RecreateGrid();

            var cell = grid.RevealCell(1, 1);

            Assert.IsNotNull(cell);
            Assert.IsTrue(cell.Revealed);

            cell = grid.RevealCell(1, 1);
            Assert.IsNull(cell);
        }
    }
}
