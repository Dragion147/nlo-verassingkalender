using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using VerassingsKalender.Interfaces;
using VerassingsKalender.Models;
using VerassingsKalender.Repositories;

namespace VerassingsKalender.Test
{
    [TestClass]
    public class GameManagerTests
    {
        [TestMethod]
        public void TestGameStart()
        {
            var gameOptions = new GameOptions()
            {
                GridOptions = new GridOptions()
                {
                    Height = 10,
                    Width = 10,
                    Prizes = new List<Prize>()
                    {
                        new Prize()
                        {
                            Amount = 10,
                            Frequency = 1,
                        },
                    }
                }
            };
            var mockOptions = new Mock<IOptions<GameOptions>>();
            mockOptions.Setup(options => options.Value).Returns(gameOptions);

            IGameManager gm = new GameManager(mockOptions.Object, new GridFactory());
            Assert.IsTrue(gm.IsGameInProgress);
        }

        [TestMethod]
        public void TestRepeatedMove()
        {
            var gameOptions = new GameOptions()
            {
                GridOptions = new GridOptions()
                {
                    Height = 10,
                    Width = 10,
                    Prizes = new List<Prize>()
                    {
                        new Prize()
                        {
                            Amount = 10,
                            Frequency = 1,
                        },
                    }
                }
            };
            var mockOptions = new Mock<IOptions<GameOptions>>();
            mockOptions.Setup(options => options.Value).Returns(gameOptions);

            IGameManager gm = new GameManager(mockOptions.Object, new GridFactory());
            Assert.IsTrue(gm.IsValidMove(1, 1, "test"));
            Assert.IsNotNull(gm.RevealCellInGrid(1, 1, "test"));
            Assert.IsFalse(gm.IsValidMove(1, 1, "test"));
            Assert.IsFalse(gm.IsValidMove(2, 1, "test"));
            Assert.IsNull(gm.RevealCellInGrid(1, 1, "test"));


            Assert.IsTrue(gm.IsValidMove(2, 1, "bob"));
            Assert.IsNotNull(gm.RevealCellInGrid(2, 1, "bob"));
        }
    }
}
