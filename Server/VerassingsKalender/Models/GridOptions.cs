﻿using System.Collections.Generic;

namespace VerassingsKalender.Models
{
    public class GridOptions
    {
        /// <summary>
        /// Width of the grid
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Height of the grid
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Collection of prizes that may be found in the grid
        /// </summary>
        public List<Prize> Prizes { get; set; }
    }
}
