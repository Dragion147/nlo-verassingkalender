﻿namespace VerassingsKalender.Models
{
    public class Prize
    {
        /// <summary>
        /// The value of the prize or prize amount
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// How often should this prize occur
        /// </summary>
        public int Frequency { get; set; }
    }
}
