﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerassingsKalender.Models
{
    public class Cell
    {
        /// <summary>
        /// The prize assigned to the cell
        /// </summary>
        public int? Prize { get; set; }

        /// <summary>
        /// Is the cell already revealed and thus the prize is already earned
        /// </summary>
        public bool Revealed { get; set; }

        /// <summary>
        /// The given X coordinate of the cell. Added for demonstration purpose
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// The given Y coordinate of the cell. Added for demonstration purpose
        /// </summary>
        public int Y { get; set; }
    }
}
