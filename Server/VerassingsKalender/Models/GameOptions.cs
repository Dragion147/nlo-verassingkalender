﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerassingsKalender.Models
{
    public class GameOptions
    {
        /// <summary>
        /// The grid options that should be used in the game
        /// </summary>
        public GridOptions GridOptions { get; set; }
    }
}
