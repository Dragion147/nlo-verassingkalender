﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerassingsKalender.Interfaces;
using VerassingsKalender.Models;

namespace VerassingsKalender.Models
{
    public class Grid : IGrid
    {
        private Random _rng = new Random();
        public List<List<Cell>> Cells { get; private set; }
        private GridOptions _options;

        public bool IsValidMove(int x, int y)
        {
            return x > 0 && y > 0 && Cells.Count >= x && Cells[0].Count >= y && !Cells[x][y].Revealed;
        }

        public Cell RevealCell(int x, int y)
        {
            Cell cell = null;

            if (IsValidMove(x, y))
            {
                cell = Cells[x][y];

                if (cell.Revealed)
                {
                    return null;
                }

                cell.Revealed = true;
            }

            return cell;
        }

        public Grid(GridOptions options)
        {
            _options = options;
        }

        public void RecreateGrid()
        {
            if (_options.Height < 0 || _options.Width < 0 || (_options.Prizes != null && _options.Prizes.Sum(x => x.Frequency) > _options.Width * _options.Height))
            {
                throw new ArgumentException();
            }

            var prizes = _options.Prizes?.Select(x => new Prize { Frequency = x.Frequency, Amount = x.Amount }).ToList();
            int totalPrizes = prizes != null ? prizes.Sum(x => x.Frequency) : 0;
            int width = _options.Width;
            int height = _options.Height;
            int totalCells = width * height;

            Cells = new List<List<Cell>>();

            for (int i = 0; i < width; i++)
            {
                Cells.Add(new List<Cell>());

                for (int j = 0; j < height; j++)
                {
                    var cell = new Cell() { Prize = 0, X = i, Y = j };
                    Cells[i].Add(cell);

                    var prizeIndex = _rng.Next(0, totalCells);
                    if (prizeIndex < totalPrizes)
                    {
                        for (int n = 0; n < prizes.Count; n++)
                        {
                            var prizeEntry = prizes[n];

                            if (prizeEntry.Frequency > 0 && prizeIndex - prizeEntry.Frequency <= 0)
                            {
                                cell.Prize = prizeEntry.Amount;
                                prizeEntry.Frequency -= 1;
                                break;
                            }
                            else
                            {
                                prizeIndex -= prizeEntry.Frequency;
                            }
                        }

                        totalPrizes--;

                    }

                    totalCells--;
                }
            }
        }
    }
}
