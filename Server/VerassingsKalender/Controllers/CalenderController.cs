﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerassingsKalender.Interfaces;
using VerassingsKalender.Models;

namespace VerassingsKalender.Controllers
{
    [Route("Calender")]
    public class CalenderController : ControllerBase
    {
        private readonly IGameManager _gameManager;
        private readonly ILogger<CalenderController> _logger;

        public CalenderController(IGameManager gameManager, ILogger<CalenderController> logger)
        {
            _gameManager = gameManager;
            _logger = logger;
        }

        /// <summary>
        /// Get the current state of the game
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(Grid), 200)]
        [ProducesResponseType(500)]
        [Route("")]
        public IActionResult GetGameGrid()
        {
            try
            {
                //Grid return in full for demonstration purpose, ie to be able to see which cell has the prizes. In real environment should only return the grid + revealed prizes
                return Ok(_gameManager.GetGrid());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unexpected exeception occured while attemping to get the grid");
            }

            return StatusCode(500);
        }

        /// <summary>
        /// Open a cell in the grid for a player.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(Cell), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Route("cell")]
        public IActionResult OpenGridCell([FromQuery]int x, [FromQuery]int y, [FromQuery] string player)
        {
            try
            {
                if (!(_gameManager.IsGameInProgress && _gameManager.IsValidMove(x, y, player)))
                {
                    return BadRequest();
                }

                return Ok(_gameManager.RevealCellInGrid(x, y, player));
            }
            catch (Exception ex)
            { 
                _logger.LogError(ex, "Unexpected exeception occured while attemping to open a cell");
            }

            return StatusCode(500);

        }
    }
}
