﻿using System.Collections.Generic;
using VerassingsKalender.Models;

namespace VerassingsKalender.Interfaces
{
    public interface IGrid
    {
        /// <summary>
        /// Readonly collection of cells. Advised to use <see cref="IsValidMove(int, int)"/> and/or <see cref="RevealCell(int, int)"/> to interact with the grid.
        /// Main purpose is to serve as a model as output in the controller.
        /// </summary>
        List<List<Cell>> Cells { get; }

        /// <summary>
        /// Test if move with coordinates <paramref name="x"/> and <paramref name="y"/> is valid.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>True if valid, false if invalid.</returns>
        bool IsValidMove(int x, int y);

        /// <summary>
        /// Create or Recreate the grid.
        /// </summary>
        void RecreateGrid();

        /// <summary>
        /// Reveal a cell in the grid by coordinates <paramref name="x"/> and <paramref name="y"/>.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns><see cref="Cell"/> or null if invalid move</returns>
        Cell RevealCell(int x, int y);
    }
}