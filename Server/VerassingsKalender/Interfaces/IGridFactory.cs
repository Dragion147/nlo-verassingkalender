﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerassingsKalender.Models;

namespace VerassingsKalender.Interfaces
{
    public interface IGridFactory
    {
        /// <summary>
        /// Create a grid using the provided options.
        /// </summary>
        /// <param name="gridOptions"></param>
        /// <returns></returns>
        IGrid CreateGrid(GridOptions gridOptions);
    }
}
