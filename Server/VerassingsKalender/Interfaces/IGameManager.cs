﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerassingsKalender.Models;

namespace VerassingsKalender.Interfaces
{
    public interface IGameManager
    {
        /// <summary>
        /// Get the current game grid state.
        /// </summary>
        /// <returns></returns>
        IGrid GetGrid();

        /// <summary>
        /// Reveal a cell for player at supplied coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        Cell RevealCellInGrid(int x, int y, string player);

        /// <summary>
        /// Is a game currently active.
        /// </summary>
        bool IsGameInProgress { get; }

        /// <summary>
        /// Is the provided move for coordinates by played a valid move in the current game.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        bool IsValidMove(int x, int y, string player);
    }
}
