﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerassingsKalender.Interfaces;
using VerassingsKalender.Models;

namespace VerassingsKalender
{
    public class GameManager : IGameManager
    {
        private bool _isGameInProgress = false;
        private IGrid _currentGrid;
        private HashSet<string> _participators;
        private IGridFactory _gridFactory;
        private GameOptions _options;

        public GameManager(IOptions<GameOptions> options, IGridFactory gridFactory)
        {
            _gridFactory = gridFactory;
            _options = options.Value;
            CreateGrid();
        }

        public bool IsGameInProgress => _isGameInProgress;

        public IGrid GetGrid()
        {
            return IsGameInProgress ? _currentGrid : null;
        }

        public bool IsValidMove(int x, int y, string player)
        {
            if (!IsGameInProgress || PlayerHasParticipated(player)) return false;

            return _currentGrid.IsValidMove(x, y);
        }

        private bool PlayerHasParticipated(string player)
        {
            return _participators.Contains(player);
        }

        public Cell RevealCellInGrid(int x, int y, string player)
        {
            if (PlayerHasParticipated(player)) { return null; }
            _participators.Add(player);
            return _currentGrid.RevealCell(x, y);
        }

        private void CreateGrid()
        {
            _participators = new HashSet<string>();
            _currentGrid = _gridFactory.CreateGrid(_options.GridOptions);
            _currentGrid.RecreateGrid();
            _isGameInProgress = true;
        }
    }
}
