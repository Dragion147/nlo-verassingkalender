﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerassingsKalender.Interfaces;
using VerassingsKalender.Models;

namespace VerassingsKalender.Repositories
{
    public class GridFactory : IGridFactory
    {
        public IGrid CreateGrid(GridOptions gridOptions)
        {
            return new Grid(gridOptions);
        }
    }
}
